#!/usr/bin/python3
# -*- coding: utf-8 -*- #

AUTHOR = 'Will'
SITENAME = "Will's Blog"
SITEURL = ''
TIMEZONE = 'UTC'
DEFAULT_LANG = 'zh-cn'
DEFAULT_PAGINATION = 5
USE_FOLDER_AS_CATEGORY = False
DELETE_OUTPUT_DIRECTORY = True

PATH = 'content'
OUTPUT_PATH = 'public'
PATH_METADATA = 'posts\/(?P<date>\d{4}\/\d{2}\/\d{2})\/(?P<slug>.*)\/(?P<name>.*)\..*'
ARTICLE_PATHS = ['posts']
ARTICLE_SAVE_AS = ARTICLE_URL = 'posts/{date:%Y}/{date:%m}/{date:%d}/{slug}/{name}.html'

STATIC_PATHS = [
    'posts/2020/10/04/bi_bao_zhi_shu/img',
]

# Feed generation is usually not desired when developing
FEED_ALL_ATOM = None
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None

# Blogroll
# LINKS = (('Pelican', 'http://getpelican.com/'),
#          ('Python.org', 'http://python.org/'),
#          ('Jinja2', 'http://jinja.pocoo.org/'),
#          ('You can modify those links in your config file', '#'),)

# Social widget
# SOCIAL = (
#     ('Github', 'coderdreamer@gmail.com'),
# )

SUMMARY_MAX_LENGTH = 50
DISQUS_SITENAME = 'will-s-blog'

THEME = 'blue_penguin'

# code blocks with line numbers
PYGMENTS_RST_OPTIONS = {'linenos': 'table'}
